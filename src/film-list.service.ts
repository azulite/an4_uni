import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Film } from './model/film';
// import { RentalOverdue } from './model/rental-overdue';
import { TxRecord } from './model/txRecord';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FilmListService {


  constructor(private http: Http) { }

  //private dbq3 = 'http://express.jakeh.io/antests/txapi/3055010000';
  private auto_complete = 'http://localhost:3006/en/antests/autoapi/';
  private tx_records = 'http://localhost:3006/en/antests/txapi/';
  private relative_ests = 'http://localhost:3006/en/antests/estsapi/';


  getAutoComplete(term): Observable<TxRecord[]> {
    return this.http.get(this.auto_complete+term)
      // ...and calling .json() on the response to return data
      // .map((res: Response) => res.json().map((item:Object)=>Object.keys(item)[0]))
      .map(this.extractAutoCompleteData) //for test out the res data structure
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error: getAutoComplete'));
  }

  getTxRecords(estID,page): Observable<TxRecord[]> {
    return this.http.get(this.tx_records+estID+'/'+page) 
      // ...and calling .json() on the response to return data
      // .map((res: Response) => res.json())//.map((item:Object)=>Object.keys(item)[0]))
      .map(this.extractTxRecords) //for test out the res data structure
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error: getTxRecords'));
  }

  getRelativeEsts(distID): Observable<TxRecord[]> {
    return this.http.get(this.relative_ests+distID) 
      // ...and calling .json() on the response to return data
      // .map((res: Response) => res.json())//.map((item:Object)=>Object.keys(item)[0]))
      .map(this.extractRelativeEsts) //for test out the res data structure
      // ...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error: getAutoComplete'));
  }

  // for test out the res data structure

  private extractAutoCompleteData(res: Response) {
    // let body = res.json();
    // console.log('body: ',body);
    const newArray = res.json().map((item:Object)=>Object.keys(item)[0]);
    // console.log('newArray: ',newArray);
    return res.json() || { };
  }

  private extractTxRecords(res: Response) {
    // console.log('res: ',res);
    return {tx:res.json().tx,txcount:res.json().txcount}|| { };
    // return res.json().tx || { };
  }

  private extractRelativeEsts(res: Response) {
    // console.log("extractRelativeEsts: ",res);
    return res.json()|| { };
  }


}
