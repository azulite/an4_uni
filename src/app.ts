import { Observable } from 'rxjs/Rx';
import { ENGINE_METHOD_PKEY_ASN1_METHS } from 'constants';
import { NgModule, Component, Inject, OnInit, Input, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, Router } from '@angular/router'
import { HttpModule, Http } from '@angular/http';
import { FormsModule } from '@angular/forms'
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import { FormsModule, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms'

import { FilmListService } from './film-list.service';
import { TxRecord } from './model/txRecord';
// import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
// import { HeaderComponent } from './header/header.component';

export function HttpLoaderFactory(http: Http) {
  // return new TranslateHttpLoader(http, "/public/lang-files/", "-lang.json");
  return new TranslateHttpLoader(http);
}

@Component({
  selector: 'app-header',
  template: `<h1>{{title1}}</h1>`
})

export class HeaderComponent {
	title1 = '4|\\|6|_|14|2 2';
}

@Component({
  selector: 'app-footer',
  template: `<p>I am footer</p>`
})

export class FooterComponent { }

@Component({
	changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
	selector: 'home-view',
	template: `
	<div _ngcontent-ele-68="" class="row">
  <div _ngcontent-ele-68="" class="col-sm-8">
	<div>
		<button (click)="switchp1()">p1</button>
		<button (click)="switchp2()">p2</button>
		<form #f="ngForm" (ngSubmit)="changeEstid()">
			<input name="newestid" [(ngModel)]="newestidval">
			<button>Submit</button>
		</form>
	</div>
    <h2 _ngcontent-ele-68="">Transaction Table</h2>
    <table class="table">
      <thead>
        <tr>
          <th>大廈</th>
          <th>室</th>
          <th>面積(實,呎)</th>
          <th>面積(建,呎)</th>
          <th>成交價</th>
        </tr>
      </thead>
      <tbody class="table">
        <tr *ngFor="let txRecord of txRecords">
          <td>{{txRecord.BLDG_CHI_NAME}}</td>
          <td>{{txRecord.FLAT}}</td>
          <td>{{txRecord.GROSS_AREA}}</td>
          <td>{{txRecord.NET_AREA}}</td>
          <td>{{txRecord.SELL}}</td>
        </tr>
      </tbody>
    </table>
    <div _ngcontent-ele-68="" class="col-md-6 pagination-custom ">
        
    
 
    </div>
  </div>
  <div _ngcontent-ele-68="" class="col-sm-4">
    <h2 _ngcontent-ele-68="">{{ 'Other Estates' | translate }}</h2>
    <table class="table">
      <thead>
        <tr>
          <th>屋苑 名稱</th>
        </tr>
      </thead>
      <tbody class="table">
        <tr *ngFor="let relativeEst of relativeEsts">
          <td>{{relativeEst.EST_CHI_NAME}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
	`
})

export class HomeView implements OnInit {
	page = 1;
  // txRecords: Observable<Array<any>>;
	
	newestid: string;
	newestidval = '';
  pageSize = 0;
  model: any;
  tx_model: any;
  searching = false;
  searchFailed = false;
  relativeEsts: any;
	// estidinput = new FormControl();
	
	@Input() selectedEst:any;
	@Input() txRecords: TxRecord[];

	constructor(
		@Inject('filmList') private filmList,
		private translate: TranslateService,
		private route: Router
	) {
		translate.setDefaultLang('zh');
    translate.use('zh');
		// let url1 = this.route.url.split('/');
    // console.log('url: ' + url1);
    // console.log('url 1: ' + url1[1]);
    // if (url1[1] === 'en') {
    //   translate.setDefaultLang('en');
    //   translate.use('en');
    //   // this.flangestname = 'EST_ENG_NAME';
    // } else {
    //   translate.setDefaultLang('zh');
    //   translate.use('zh');
    //   // this.flangestname = 'EST_CHI_NAME';
    // }
	}
	
  ngOnInit() {
		// this.addItemStream.subscribe(() => {
		// 	this.pageChanged();
		// })
		this.pageChanged();
    this.loadRelativeEsts();
  }

	switchp1() {
		this.selectedEst = 'E12603';
		console.log('btn 1 pressed');
    this.updatetb1(this.selectedEst);
  }

  switchp2() {
		this.selectedEst = 'E12477';
		console.log('btn 2 pressed');
    this.updatetb1(this.selectedEst);
  }

	pageChanged() {
    let targetEstID: string;
    if (!this.selectedEst) {
      targetEstID = 'E12603';
      console.log('targetEstID default: ', targetEstID);
    } else {
      // console.log('this.selectedEst[0]: ',this.selectedEst[0]);
      // targetEstID = this.selectedEst[Object.keys(this.selectedEst)[0]];
			targetEstID = this.selectedEst;
      console.log('targetEstID: ', targetEstID);
    }
    console.log("newPage: ", this.page);
    this.filmList.getTxRecords(targetEstID.trim(), this.page)
      .subscribe(
      txRecords => {
				this.changePage(txRecords) //this.txRecords = txRecords.tx, // Bind to view
			},
      err => {
        // Log errors if any
        console.log(err);
      });
  }

	updatetb1(estId) {
		this.filmList.getTxRecords(estId.trim(), this.page)
      .subscribe(
      txRecords => this.changePage(txRecords),//this.txRecords = txRecords.tx, // Bind to view
      err => {
        // Log errors if any
        console.log(err);
      });
	}

	changeEstid() {
		console.log('submitted evt');
		this.filmList.getTxRecords(this.newestidval.trim(), this.page)
      .subscribe(
      txRecords => this.changePage(txRecords),//this.txRecords = txRecords.tx, // Bind to view
      err => {
        // Log errors if any
        console.log(err);
      });
	}

	private changePage(txRecords) {
    this.txRecords = txRecords.tx;
    this.pageSize = txRecords.txcount;
  }

	loadRelativeEsts(){
    let targetDistID: string;
		targetDistID = '1010030000';
    // if (!this.selectedEst) {
    //   targetDistID = '1010030000';
    //   // console.log('targetDistID default: ', targetDistID);
    // } else {
    //   targetDistID = this.selectedEst["DIST_ID"];
    //   // console.log('targetDistID: ', targetDistID);
    // }
    this.filmList.getRelativeEsts(targetDistID.trim())
      .subscribe(
      relativeEsts =>  this.relativeEsts = relativeEsts, // Bind to view
      err => {
        console.log(err);
      });
  }
	
	onKey(event: any) { // without type info
    this.selectedEst = event.target.selectedEst;
		console.log(this.selectedEst);
		this.pageChanged();
  }

}

@Component({
	selector: 'demo-app',
	template: `
		<app-header></app-header>
	  <h1>Universal Demo</h1>
	  <a routerLink="/">Home</a>
	  <a routerLink="/lazy">Lazy</a>
	  <router-outlet></router-outlet>
		<app-footer></app-footer>
	`
})
export class AppComponent {
	constructor(
		@Inject('filmList') private filmList,
	) {}
}

@NgModule({
	imports: [
		// HeaderComponent,
		BrowserModule.withServerTransition({
		  appId: 'universal-demo-app'
		}),
		RouterModule.forRoot([
			{ path: '', component: HomeView, pathMatch: 'full'},
			{ path: 'lazy', loadChildren: './lazy.module#LazyModule'}
		]),
		FormsModule,
		HttpModule,
		TranslateModule.forRoot()
		// ReactiveFormsModule
		// NgbPaginationModule.forRoot(),
	],
	declarations: [ AppComponent, HomeView, HeaderComponent, FooterComponent ],
	providers: [
		{ provide: 'filmList', useClass: FilmListService }
	],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
