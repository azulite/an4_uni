export class TxRecord {
    constructor(
        public BLDG_CHI_NAME: string,
        public BLDG_ENG_NAME: string,
        public BLDG_ID: string,
        public BLOCK: string,
        public DIST_CHI_NAME: string,
        public DIST_ENG_NAME: string,
        public DIST_ID: string,
        public EST_CHI_NAME: string,
        public EST_ENG_NAME: string,
        public EST_ID: string,
        public FLAT: string,
        public GROSS_AREA: string,
        public NET_AREA: string,
        public REGION_ID: string,
        public SELL: string,
        public TRANS_DATE: Date
        ) {}
}