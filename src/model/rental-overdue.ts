export class RentalOverdue {
    constructor(
        public customer: string, 
        public phone: string, 
        public title: string,
        public rental_date: string,
        public return_date: string
        ){}
}